<?php declare(strict_types = 1);

use Lister\Vars\Data;
use Lister\Vars\DataProxy;

class DataTest extends PHPUnit_Framework_TestCase
{

    public function testSetupByMethod()
    {
        $expected = $data1 = [1, 2, 3];
        $d = new Data;
        // Normal
        $d->setup($data1);
        $this->assertSame($expected, $d->setup());
        // Bind
        $data2 = [4, 5, 6];
        $expected['test']['foo'] = $data2;
        $b = $d->bind('test.foo');
        $b->setup($data2);
        $this->assertSame($data2, $b->setup());
        $this->assertSame($expected, $d->setup());
    }

    public function testSetup()
    {
        $expected = $data1 = [1, 2, 3];
        $d = new Data;
        // Normal
        $d->setup($data1);
        $this->assertSame($expected, $d->setup());
        // Bind
        $data2 = [4, 5, 6];
        $expected['test']['foo'] = $data2;
        $b = $d->bind('test.foo');
        $b->setup($data2);
        $this->assertSame($data2, $b->setup());
        $this->assertSame($expected, $d->setup());
    }

    public function testSetupByConstructor()
    {
        $expected = $data = [1, 2, 3];
        $d = new Data($data);
        $this->assertSame($expected, $d->setup());
    }

    public function testSetupAfterSetup()
    {
        $data1 = [1, 2, 3];
        $data2 = [4, 5, 6];
        $expected = array_merge($data1, $data2);
        // Normal
        $d = new Data($data1);
        $this->assertSame($data1, $d->setup());
        $d->setup($data2);
        $this->assertSame($expected, $d->setup());
    }

    public function testAssign()
    {
        $data1 = [1, 2, 3];
        $expected = $data2 = [4, 5, 6];
        $d = new Data($data1);
        // Normal
        $d->assign($data2);
        $this->assertSame($expected, $d->setup());
        // Bind
        $b = $d->bind('test.foo');
        $b->setup($data1);
        $b->assign($data2);
        $expected['test']['foo'] = $data2;
        $this->assertSame($data2, $b->setup());
        $this->assertSame($expected, $d->setup());
        // Empty
        $d->assign();
        $this->assertSame([], $d->setup());
        $b->assign();
        $this->assertSame([], $b->setup());
        $expected = ['test' => ['foo' => []]];
        $this->assertSame($expected, $d->setup());
    }

    public function testDefaults()
    {
        $data1 = ['a' => 1, 'b' => 2];
        $defaults = ['a' => 0, 'c' => 3];
        $expected = array_merge($defaults, $data1);
        $d = new Data($data1);
        // Normal
        $d->defaults($defaults);
        $this->assertSame($expected, $d->setup());
        // Bind
        $data2 = ['a' => 3, 'b' => 4];
        $expected['test']['foo'] = array_merge($defaults, $data2);
        $b = $d->bind('test.foo');
        $b->setup($data2);
        $b->defaults($defaults);
        $this->assertSame($expected['test']['foo'], $b->setup());
        $this->assertSame($expected, $d->setup());
    }

    public function testOffsetGet()
    {
        $data = ['key1' => 'value', 'key2' => ['key3' => [1, 2, 3]], 'false' => false];
        $d = new Data($data);
        // Normal
        $this->assertSame('value', $d['key1']);
        $this->assertSame(2, $d['key2.key3.1']);
        $this->assertSame([1, 2, 3], $d['key2.key3:']);
        $this->assertNull($d['nonexistent']);
        $this->assertSame([], $d['nonexistent:']);
        $this->assertSame([], $d['nonexistent.nonexistent:']);
        $this->assertFalse($d['nonexistent?']);
        $this->assertTrue($d['false?']);
        // Bind
        $b = $d->bind('key2');
        $this->assertSame(2, $b['key3.1']);
        $this->assertTrue($b['key3.2?']);
        $this->assertFalse($b['key3.3?']);
        $this->assertSame('value', $b['#key1']);
    }

    public function testOffsetSet()
    {
        $expected = ['key1' => 'value', 'key2' => ['key3' => [1, 2, 3]]];
        // Normal
        $d = new Data();
        $d['key1'] = 'value';
        $d['key2.key3>'] = 2;
        $d['key2.key3<'] = 1;
        $d['key2.key3>'] = 3;
        $this->assertSame($expected, $d->setup());
        // Bind
        $b = $d->bind('key2');
        $b['key1'] = 'value';
        $b['key2.key3>'] = 2;
        $b['key2.key3<'] = 1;
        $b['key2.key3>'] = 3;
        $b['#key1'] = 'bind';
        $tmp = array_merge($expected['key2'], $expected);
        $this->assertSame($tmp, $b->setup());
        $tmp = $expected;
        $tmp['key2'] = array_merge($expected['key2'], $expected);
        $tmp['key1'] = 'bind';
        $this->assertSame($tmp, $d->setup());
    }

    public function testOffsetUnset()
    {
        $data = ['key1' => 'value', 'key2' => ['key3' => [1, 2, 3]]];
        $expected = $data;
        // Normal
        $d = new Data($data);
        unset($d['key1']);
        unset($expected['key1']);
        $this->assertSame($expected, $d->setup());
        unset($d['key2.key3.1']);
        unset($expected['key2']['key3'][1]);
        $this->assertSame($expected, $d->setup());
        // Binded
        $b = $d->bind('key2');
        unset($b['key3.0']);
        $expected = $expected['key2'];
        unset($expected['key3'][0]);
        $this->assertSame($expected, $b->setup());
        unset($b['#key1']);
        $this->assertFalse($d['key1?']);
    }

    public function testAsString()
    {
        // Normal
        $d = new Data(['a' => [1, 2, 3]]);
        $expected = "array (\n  'a' => \n  array (\n    0 => 1,\n    1 => 2,\n    2 => 3,\n  ),\n)";
        $this->assertSame($expected, (string)$d);
        $this->assertSame($expected, $d->asString());
        // Binded
        $b = $d->bind('a');
        $expected = "array (\n  0 => 1,\n  1 => 2,\n  2 => 3,\n)";
        $this->assertSame($expected, (string)$b);
        $this->assertSame($expected, $b->asString());
    }

    public function testBind()
    {
        // Normal bind
        $expected = [1, 2, 3];
        $data = ['key1' => 'value', 'key2' => ['key3' => $expected]];
        $d = new Data($data);
        $b = $d->bind('key2.key3');
        $this->assertInstanceOf(DataProxy::class, $b);
        $this->assertSame([1, 2, 3], $b->setup());
        // Test iterator
        $tmp = '';
        foreach ($b as $key => $value) {
            $tmp .= $value;
        }
        $this->assertSame('123', $tmp);
        // Binded bind
        $a = $b->bind('key4.key5');
        $this->assertInstanceOf(DataProxy::class, $a);
        $a->setup($expected);
        $this->assertSame([1, 2, 3], $a->setup());
        // Test iterator
        $tmp = '';
        foreach ($a as $key => $value) {
            $tmp .= $value;
        }
        $this->assertSame('123', $tmp);
    }

    public function testBindNonexistent()
    {
        $data = ['key1' => 'value', 'key2' => ['key3' => [1, 2, 3]]];
        $d = new Data($data);
        $b = $d->bind('nonexistent.nonexistent');
        $this->assertInstanceOf(DataProxy::class, $b);
        $this->assertSame([], $b->setup());
        $this->assertNull($b['nonexistent']);
        $tmp = false;
        foreach ($b as $key => $value) {
            $tmp = true;
        }
        $this->assertFalse($tmp);
        $b['test'] = 'value';
        $this->assertSame(['test' => 'value'], $b->setup());
        $this->assertSame(['test' => 'value'], $d->setup('nonexistent.nonexistent'));
    }

    public function testBindNonStdKey()
    {
        $data = [
            0 => ['i' => 'zero'],
            1 => ['i' => 'one'],
            2 => ['i' => 'two'],
            '' => ['i' => 'three']
        ];
        $d = new Data($data);
        $tests = [
            // Cast to 0
            'zero' => false,
            // Cast to 1
            'one' => true,
            // Cast to 2
            'two' => 2.789,
            // Cast to ''
            'three' => null,
        ];
        foreach ($tests as $result => $key) {
            $b = $d->bind($key);
            $this->assertInstanceOf(DataProxy::class, $b);
            $this->assertSame($result, $b['i']);
        }
    }

    public function testBindIllegalOffsetObject()
    {
        $d = new Data;
        // Suppress error
        $b = @$d->bind(new StdClass);
        $this->assertInstanceOf(DataProxy::class, $b);
        $this->assertSame('', $b->bind);
        // Raise error
        $this->expectException('PHPUnit_Framework_Error_Warning');
        $this->expectExceptionMessage('Illegal offset type');
        $b = $d->bind(new StdClass);
    }

    public function testBindIllegalOffsetArray()
    {
        $d = new Data;
        // Suppress error
        $b = @$d->bind([]);
        $this->assertInstanceOf(DataProxy::class, $b);
        $this->assertSame('', $b->bind);
        // Raise error
        $this->expectException('PHPUnit_Framework_Error_Warning');
        $this->expectExceptionMessage('Illegal offset type');
        $b = $d->bind([]);
    }

}