<?php declare(strict_types = 1);

use Lister\Vars\Fluent;

class FluentTest extends PHPUnit_Framework_TestCase
{

    public function testConstructor()
    {
        $expected = ['foo' => 'baz'];
        $f = new Fluent($expected);
        $this->assertSame($expected, $f->getAttributes());
    }

    public function testFluent()
    {
        $expected = [
            'foo1' => 2,
            'foo2' => true,
            'foo3' => [1, 2, 3],
            'foo4' => 'a',
        ];
        $f = new Fluent;
        // Common usage
        $f->foo1(1)->foo2()->foo3([1, 2, 3]);
        // Ignore everything but first argument
        $f->foo4('a', 'b', 'c');
        // Overwrite
        $f->foo1(2);
        $this->assertSame($expected, $f->getAttributes());
    }

    public function testArrayAccess()
    {
        $f = new Fluent(['foo' => 'baz']);
        $this->assertFalse(isset($f['nan']));
        $this->assertNull($f['nan']);
        $this->assertTrue(isset($f['foo']));
        $this->assertSame('baz', $f['foo']);
        unset($f['foo']);
        $this->assertNull($f['foo']);
    }

}