<?php declare(strict_types = 1);

use Lister\Vars\Strings as str;

class StringsTest extends PHPUnit_Framework_TestCase
{

    protected $utf8tests = [
        // Valid ASCII
        'ABC123' => true,
        // Valid 2 Octet Sequence
        "\xC3\xB1" => true,
        // Invalid 2 Octet Sequence
        "\xC3\x28" => false,
        // Invalid Sequence Identifier
        "\xA0\xA1" => false,
        // Valid 3 Octet Sequence
        "\xE2\x82\xA1" => true,
        // Invalid 3 Octet Sequence (in 2nd Octet)
        "\xE2\x28\xA1" => false,
        // Invalid 3 Octet Sequence (in 3rd Octet)
        "\xE2\x82\x28" => false,
        // Valid 4 Octet Sequence
        "\xF0\x90\x8C\xBC" => true,
        // Invalid 4 Octet Sequence (in 2nd Octet)
        "\xF0\x28\x8C\xBC" => false,
        // Invalid 4 Octet Sequence (in 3rd Octet)
        "\xF0\x90\x28\xBC" => false,
        // Invalid 4 Octet Sequence (in 4th Octet)
        "\xF0\x28\x8C\x28" => false,
        // Valid 5 Octet Sequence (but not Unicode!)
        "\xF8\xA1\xA1\xA1\xA1" => false,
        // Valid 6 Octet Sequence (but not Unicode!)
        "\xFC\xA1\xA1\xA1\xA1\xA1" => false,
    ];

    public function testIsAscii()
    {
        // Valid ASCII
        $this->assertTrue(str::isAscii('ABC123'));
        // First and last char
        $this->assertTrue(str::isAscii("\x00\x7F"));
        // First of extended ASCII table
        $this->assertFalse(str::isAscii("\x80"));
        // Last of extended ASCII table
        $this->assertFalse(str::isAscii("\xFF"));
    }

    public function testIsUtf8()
    {
        $tests = $this->utf8tests;
        foreach ($tests as $text => $result)
            if ($result)
                $this->assertTrue(str::isUtf8($text));
            else
                $this->assertFalse(str::isUtf8($text));
    }

    public function testIsMb()
    {
        $tests = $this->utf8tests;
        // Pure ASCII is not multibyte
        $tests['ABC123'] = false;
        // Extended ASCII is not multibyte either
        $tests["\x80"] = false;
        foreach ($tests as $text => $result)
            if ($result)
                $this->assertTrue(str::isMb($text));
            else
                $this->assertFalse(str::isMb($text));
    }

    public function testAscii()
    {
        // btw. http://www.theworldofstuff.com/characters/
        $tests = [
            'ABC123' => 'ABC123',
            "\x00\x01" => "\x00\x01",
            'aÁáČčĎďÉéĚěÍíŇňÓóŘřŠšŤťÚúŮůÝýŽž'
            => 'aAaCcDdEeEeIiNnOoRrSsTtUuUuYyZz',
        ];
        foreach ($tests as $text => $expected)
            $this->assertSame($expected, str::ascii($text));
        // Different encoding
        $cp1250 = "\x61\xC1\xE1\xC8\xE8\xCF\xEF\xC9\xE9\xCC\xEC\xCD\xED\xD2\xF2\xD3\xF3\xD8\xF8\x8A\x9A\x8D\x9D\xDA\xFA\xD9\xF9\xDD\xFD\x8E\x9E";
        $this->assertSame('aAaCcDdEeEeIiNnOoRrSsTtUuUuYyZz', str::ascii($cp1250, 'CP1250'));
    }

    public function testSlug()
    {
        $text = 'Author Erich_<b>Kästner</b> 1.2.3';
        $expected = 'author-erich_kastner-123';
        $this->assertSame($expected, str::slug($text));
    }

    public function testSnakeCase()
    {
        $this->assertSame('foo_pdo_exception', str::snake('FooPDOException'));
        $this->assertSame('foo_bar', str::snake('fooBar'));
        $this->assertSame('foo-bar', str::snake('fooBar', '-'));
    }

    public function testStudlyCase()
    {
        $this->assertSame('FooBar', str::studly('fooBar'));
        $this->assertSame('FooBar', str::studly('foo_bar'));
        $this->assertSame('FooBarBaz', str::studly('foo-barBaz'));
        $this->assertSame('FooBarBaz', str::studly('foo-bar_baz'));
    }

    public function testCamelCase()
    {
        $this->assertSame('fooBar', str::camel('FooBar'));
        $this->assertSame('fooBar', str::camel('foo_bar'));
        $this->assertSame('fooBarBaz', str::camel('Foo-barBaz'));
        $this->assertSame('fooBarBaz', str::camel('foo-bar_baz'));
    }

    public function testStartsWith()
    {
        $this->assertTrue(str::startsWith('taylor', 'tay'));
        $this->assertTrue(str::startsWith('taylor', ['tay']));
        $this->assertFalse(str::startsWith('taylor', 'xxx'));
        $this->assertFalse(str::startsWith('taylor', ['xxx']));
        $this->assertFalse(str::startsWith('taylor', ''));
        $this->assertFalse(str::startsWith('', ''));
    }

    public function testContains()
    {
        $this->assertTrue(str::contains('taylor', 'ylo'));
        $this->assertTrue(str::contains('taylor', ['ylo']));
        $this->assertFalse(str::contains('taylor', 'xxx'));
        $this->assertFalse(str::contains('taylor', ['xxx']));
        $this->assertFalse(str::contains('taylor', ''));
        $this->assertFalse(str::contains('', ''));
    }

    public function testEndsWith()
    {
        $this->assertTrue(str::endsWith('taylor', 'lor'));
        $this->assertTrue(str::endsWith('taylor', ['lor']));
        $this->assertFalse(str::endsWith('taylor', 'xxx'));
        $this->assertFalse(str::endsWith('taylor', ['xxx']));
        $this->assertFalse(str::endsWith('taylor', ''));
        $this->assertFalse(str::endsWith('', ''));
    }

    public function testPerc()
    {
        $this->assertSame('33', str::perc(3, 1));
        $this->assertSame('33.33', str::perc(3, 1, 2));
    }

    public function testImplode()
    {
        // Normal
        $array = ['a' => 1, 'b' => 2, 'c' => null, 'd' => 4];
        $expected = 'a="01";b="02";c="00";d="04"';
        $this->assertSame($expected, str::implode('%s="%02d"', ';', $array));
        // Format null
        $expected = 'a="01";b="02";c=NULL;d="04"';
        $result = str::implode('%s="%02d"', ';', $array, '%s=NULL');
        $this->assertSame($expected, $result);
    }

    public function testExplode()
    {
        $delimiter = ' ';
        $count = range(1, 5);
        foreach ($count as $c) {
            $string = implode($delimiter, array_fill(0, $c, 'John'));
            $result = str::explode($delimiter, $string, $c * 2);
            $this->assertCount($c * 2, $result);
            $this->assertCount($c, preg_grep('/John/', $result));
        }
    }

    public function testPrexplode()
    {
        $preg = '/\s+/';
        $delimiter = ' ';
        $count = range(1, 5);
        foreach ($count as $c) {
            $string = implode(str_repeat($delimiter, $c), array_fill(0, $c, 'John'));
            $result = str::prexplode($preg, $string, $c * 2);
            $this->assertCount($c * 2, $result);
            $this->assertCount($c, preg_grep('/John/', $result));
        }
    }

    public function testLimit()
    {
        $text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.';
        // Zero length
        $this->assertSame('', str::limit('', 1, ':', ' '));
        // Same length
        $this->assertSame($text, str::limit($text, strlen($text)));
        // Cut normally (no stopAt defined)
        $expected = 'Lor:';
        $this->assertSame($expected, str::limit($text, 3, ':', ''));
        // Cut normally (no stopAt found)
        $expected = 'Lor:';
        $this->assertSame($expected, str::limit($text, 3, ':', ' '));
        // Found stopAt at position 0
        $expected = 'Lor:';
        $this->assertSame($expected, str::limit($text, 3, ':', 'L'));
        // Last stopAt
        $expected = 'Lorem:';
        $this->assertSame($expected, str::limit($text, 7, ':', ' '));
        // Exactly at stopAt
        $expected = 'Lorem:';
        $this->assertSame($expected, str::limit($text, 6, ':', ' '));
    }

    public function testRandom()
    {
        $this->assertSame(10, strlen(str::random(10)));
        $this->assertSame('1111111111', str::random(10, '1'));
    }

    public function testNumero()
    {
        $this->assertSame('123456', str::numero("+Aě1šč23\n\t45C6x"));
    }

    public function testSameStart()
    {
        $this->assertSame('ěšč1', str::sameStart('ěšč123', 'ěšč199'));
    }

    public function testSameEnd()
    {
        $this->assertSame('123', str::sameEnd('abc123', 'def123'));
        $this->assertSame('č123', str::sameEnd('ěšč123', '12č123'));
    }

}