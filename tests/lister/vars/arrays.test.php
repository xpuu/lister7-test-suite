<?php declare(strict_types=1);

use Lister\Vars\Arrays as arr;

class ArraysTest extends PHPUnit_Framework_TestCase
{

    public function testIsAssoc()
    {
        $this->assertTrue(arr::isAssoc(['a' => 'b']));
    }

    public function testIsNotAssoc()
    {
        $this->assertFalse(arr::isAssoc([1, 2, 3]));
    }

    public function testIsMulti()
    {
        $flat = [1, 2, 3];
        $multi = [1, 2, [3, 4]];
        $this->assertFalse(arr::isMulti($flat));
        $this->assertTrue(arr::isMulti($multi));
    }

    public function testDiffMulti()
    {
        // Normal diff
        $a = [2, 3, 1];
        $b = [2, 3, 4];
        $diff = arr::diffMulti($a, $b);
        $expected = array_diff($a, $b);
        $this->assertSame($expected, $diff);
        // Multi diff
        $a = [1, 2, 3, 4, [1, 2], [3, 4]];
        $b = [1, 2, 3, [1, 2], [6, 7]];
        $diff = arr::diffMulti($a, $b);
        #var_dump($diff);
        $expected = [4, [3, 4]];
        $this->assertSame(array_values($expected), array_values($diff));
    }

    public function testDiffStrangeTypes()
    {
        $c1 = new stdClass;
        $c2 = new stdClass;
        $f1 = fopen(__FILE__, 'r');
        $f2 = fopen(__FILE__, 'r');
        $a = [$c1, $c2, $f1, $f2, [3], [2]];
        $b = [$c1, $f1, [1], [2]];
        $expected = [1 => $c2, 3 => $f2, 4 => [3]];
        $this->assertSame($expected, arr::diffMulti($a, $b));
    }

    public function testOnly()
    {
        $a = range(1, 5);
        $a = array_combine($a, $a);
        $b = array_combine(['a', 'b', 'c', 'd', 'e'], $a);
        $this->assertSame([1 => 1, 3 => 3], arr::only($a, [1, 3]));
        $this->assertSame(['b' => 2, 'd' => 4], arr::only($b, ['b', 'd']));
        $this->assertSame([], arr::only($a, ['x', 'y']));
        // Short syntax
        $this->assertSame([5 => 5], arr::only($a, 5));
    }

    public function testExcept()
    {
        $a = range(1, 5);
        $a = array_combine($a, $a);
        $b = array_combine(['a', 'b', 'c', 'd', 'e'], $a);
        $this->assertSame([2 => 2, 4 => 4, 5 => 5], arr::except($a, [1, 3]));
        $this->assertSame(['a' => 1, 'c' => 3, 'e' => 5], arr::except($b, ['b', 'd']));
        $this->assertSame($a, arr::except($a, ['x', 'y']));
        // Short syntax
        $expected = $a;
        unset($expected[5]);
        $this->assertSame($expected, arr::except($a, 5));
    }

    public function testMergeInvalidArgument()
    {
        $this->expectException('TypeError', 'Argument #1 passed to Lister\Vars\Arrays::merge() must be of the type array, integer given');
        arr::merge(1);
    }

    public function testMerge()
    {
        $a = ['key1' => 'value1'];
        $b = ['key2' => [1, 2, 3]];
        $c = ['key1' => 'value2', 'key2' => [3, 4, 5]];
        $d = ['!key1' => 'foo'];
        $merged = arr::merge($a, $b, $c, $d);
        $expected = ['key1' => 'value2', 'key2' => [1, 2, 3, 4, 5], '!key1' => 'foo'];
        $this->assertSame($expected, $merged);
    }

    public function testMergeOverwrite()
    {
        $a = ['key1' => 'value1'];
        $b = ['key2' => [1, 2, 3]];
        $c = ['key1' => 'value2', 'key2' => [3, 4, 5]];
        $d = ['!key1' => 'foo'];
        $merged = arr::merge($a, $b, $c, $d, '!');
        $expected = ['key1' => 'foo', 'key2' => [1, 2, 3, 4, 5]];
        $this->assertSame($expected, $merged);
    }

    public function testMergeStrangeTypes()
    {
        $i1 = new stdClass;
        $i2 = new stdClass;
        $f1 = fopen(__FILE__, 'r');
        $f2 = fopen(__FILE__, 'r');
        $a = ['a' => [$i1], 'b' => []];
        $b = ['a' => [$i2], 'b' => [$f1, $f2]];
        $expected = ['a' => [$i1, $i2], 'b' => [$f1, $f2]];
        $this->assertSame($expected, arr::merge($a, $b));
    }

    public function testGet()
    {
        $a = ['a' => ['b' => ['c' => 'foo'], 'i' => [1, 2, 3]], 'd' => 1, '' => ['' => 'x']];
        // Flat
        $this->assertSame(false, arr::get($a, 'nan?'));
        $this->assertSame(null, arr::get($a, 'nan'));
        $this->assertSame([], arr::get($a, 'nan:'));
        $this->assertSame('default', arr::get($a, 'nan', 'default'));
        $this->assertSame(['default'], arr::get($a, 'nan:', 'default'));
        $this->assertSame(true, arr::get($a, 'd?'));
        $this->assertSame(1, arr::get($a, 'd'));
        $this->assertSame([1], arr::get($a, 'd:'));
        // Multi
        $this->assertSame(false, arr::get($a, 'a.b.nan?'));
        $this->assertSame(null, arr::get($a, 'a.b.nan'));
        $this->assertSame([], arr::get($a, 'a.b.nan:'));
        $this->assertSame('default', arr::get($a, 'a.b.nan', 'default'));
        $this->assertSame(['default'], arr::get($a, 'a.b.nan:', 'default'));
        $this->assertSame(true, arr::get($a, 'a.b.c?'));
        $this->assertSame('foo', arr::get($a, 'a.b.c'));
        $this->assertSame(['foo'], arr::get($a, 'a.b.c:'));
        $this->assertSame('x', arr::get($a, '.'));
        // Indexed
        $this->assertSame(false, arr::get($a, 'a.i.11?'));
        $this->assertSame(null, arr::get($a, 'a.i.11'));
        $this->assertSame([], arr::get($a, 'a.i.11:'));
        $this->assertSame('default', arr::get($a, 'a.i.11', 'default'));
        $this->assertSame(['default'], arr::get($a, 'a.i.11:', 'default'));
        $this->assertSame(true, arr::get($a, 'a.i.2?'));
        $this->assertSame(3, arr::get($a, 'a.i.2'));
        $this->assertSame([3], arr::get($a, 'a.i.2:'));
    }

    public function testGetNonStdOffsetTypes()
    {
        $a = [0 => 'zero', 1 => 'one', 2 => 'two', '' => 'three'];
        $tests = [
            // Cast to 0
            'zero' => false,
            // Cast to 1
            'one' => true,
            // Cast to 2
            'two' => 2.789,
            // Cast to ''
            'three' => null,
        ];
        foreach ($tests as $result => $path) {
            $this->assertSame($result, arr::get($a, $path));
        }
    }

    public function testSet()
    {
        $a = ['a' => ['i' => [2]]];
        $tests = [
            'd' => 1,
            'a.b.c' => 'foo',
            'a.i>' => [3, [2, 3]],
            'a.i<' => [1, [1, 2]],
            '.' => 'x',
        ];
        foreach ($tests as $path => $result) {
            $set = $result;
            if (is_array($result)) {
                $set = $result[0];
                $result = $result[1];
            }
            $t = $a;
            arr::set($t, $path, $set);
            $this->assertSame($result, arr::get($t, trim($path, '<>')));
        }
    }

    public function testSetNonStdOffsetTypes()
    {
        $a = [];
        $expected = [0 => 'zero', 1 => 'one', 2 => 'two', '' => 'three'];
        $tests = [
            // Cast to 0
            'zero' => false,
            // Cast to 1
            'one' => true,
            // Cast to 2
            'two' => 2.789,
            // Cast to ''
            'three' => null,
        ];
        foreach ($tests as $result => $path) {
            arr::set($a, $path, $result);
        }
        $this->assertSame($expected, $a);
    }

    public function testUnset()
    {
        $a = ['a' => ['b' => ['c' => 'foo'], 'i' => [1, 2, 3]], 'd' => 1];
        // Flat
        $expected = $t = $a;
        arr::unset($t, 'a');
        unset($expected['a']);
        $this->assertSame($expected, $t);
        // Multi
        $expected = $t = $a;
        arr::unset($t, 'a.b.c');
        unset($expected['a']['b']['c']);
        $this->assertSame($expected, $t);
        // Indexed
        $expected = $t = $a;
        arr::unset($t, 'a.i.1');
        unset($expected['a']['i'][1]);
        $this->assertSame($expected, $t);
    }

    public function testUnsetNonStdOffsetTypes()
    {
        $a = [0 => 'zero', 1 => 'one', 2 => 'two', '' => 'three'];
        $tests = [
            // Cast to 0
            'zero' => false,
            // Cast to 1
            'one' => true,
            // Cast to 2
            'two' => 2.789,
            // Cast to ''
            'three' => null,
        ];
        foreach ($tests as $result => $path) {
            arr::unset($a, $path);
        }
        $this->assertSame([], $a);
    }

    ## ---- Filters

    public function testFilter()
    {
        $a = [0, null, 1, null, true, false];
        $filtered = arr::filter($a);
        $expected = [0, 1, true, false];
        // array_values reindexes filtered array
        $this->assertSame($expected, array_values($filtered));
    }

    public function testFilterCallback()
    {
        $a = [0, null, 1, null, true, false];
        $filtered = arr::filter($a, function ($value) {
            return is_int($value);
        });
        $expected = [0, 1];
        // array_values reindexes filtered array
        $this->assertSame($expected, array_values($filtered));
    }

    public function testFilterFlags()
    {
        $a = ['a' => 'b'];
        arr::filter($a, function ($value, $key) {
            $this->assertSame('a', $key);
            $this->assertSame('b', $value);
            $this->assertSame(2, func_num_args());
        }, ARRAY_FILTER_USE_BOTH);
        arr::filter($a, function ($key) {
            $this->assertSame('a', $key);
            $this->assertSame(1, func_num_args());
        }, ARRAY_FILTER_USE_KEY);
    }

}