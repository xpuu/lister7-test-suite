<?php declare(strict_types = 1);

use Lister\Vars\Json;

class JsonTest extends PHPUnit_Framework_TestCase
{

    public function testConfig()
    {
        // Setup in constructor
        $depth = 123;
        $json = new Json([
            'depth' => $depth
        ]);
        $this->assertEquals($depth, $json->config['depth']);
        // Setup using data class
        $depth = 321;
        $json->config['depth'] = $depth;
        $this->assertEquals($depth, $json->config['depth']);
    }

    public function testEncode()
    {
        $json = new Json(['flags' => JSON_PRETTY_PRINT]);
        $var = ['a' => 1, 'b' => 2, 'c' => 3];
        $expected = json_encode($var, JSON_PRETTY_PRINT);
        $this->assertEquals($expected, $json->encode($var));
    }

    public function testDecode()
    {
        $json = new Json;
        $text = '{"a": 1, "b": 2, "c": 3}';
        $expected = ['a' => 1, 'b' => 2, 'c' => 3];
        $this->assertEquals($expected, $json->decode($text));
    }

    public function testDepthEncodeExceeded()
    {
        $json = new Json;
        // Pass
        $var = 1;
        $expected = '1';
        $this->assertEquals($expected, $json->encode($var, 0, 1));
        // Exceed
        $this->expectException('Lister\Vars\JsonException',
            'Maximum stack depth exceeded');
        $json->encode([$var], 0, 0);
    }

    public function testDepthDecodeExceeded()
    {
        $json = new Json;
        // Pass
        $text = '1';
        $expected = 1;
        $this->assertEquals($expected, $json->decode($text, 0));
        // Exceed
        $this->expectException('Lister\Vars\JsonException',
            'Maximum stack depth exceeded');
        $json->decode("[{$text}]", 1);
    }

}