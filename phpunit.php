<?php declare(strict_types = 1);

// Set date and time
date_default_timezone_set('Europe/Prague');

// All errors on
error_reporting(-1);

// Check PHP version
if (!defined('PHP_VERSION_ID') || PHP_VERSION_ID < 70100)
    die('Require PHP 7.1 or later');

// Paths
define('BASE', __DIR__ . '/');
define('LISTER', '../lister7/');

// Snake case
function snake($text)
{
    return strtolower(preg_replace('/([a-z0-9])([A-Z])/', '$1_$2', $text));
}

// Simple autoload
spl_autoload_register(function ($class) {

    $file = LISTER . str_replace('\\', '/', snake($class)) . '.cls.php';
    if (file_exists($file))
        require $file;

});

// Composer
if (file_exists($vendor = __DIR__ . '/vendor/autoload.php'))
    require $vendor;